import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SearchCarModel } from '../models/searchCar.model';
import { AppConstants } from "./../../../app/constants/app.constants";

@Injectable({
    providedIn: 'root',
})
export class HomeService {
    homeDataSource = new Subject<any>();
    homeData$ = this.homeDataSource.asObservable();
    endPoint = AppConstants.API_ENDPOINT;
    route = 'Home/';

    constructor(private http: HttpClient) { }

    getHome() {
        return this.http.get(this.endPoint + this.route + "GetHome");
    }

    getSearchResult(searchCarModel: SearchCarModel) {
        return this.http.post(this.endPoint + this.route + "GetSearchResult", searchCarModel);
    }
}