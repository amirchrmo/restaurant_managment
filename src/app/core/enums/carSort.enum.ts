export enum CarSort {
    "Recommended" = 1,
    "SmallToLarge" = 2,
    "LargeToSmall" = 3,
    "HighestPrice" = 4,
    "LowestPrice" = 5
}
