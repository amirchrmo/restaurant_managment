import { CarModel } from './car.model';
import { HomeModel } from './home.model';
import { PlaceModel } from './place.model';

export class HomeDataModel {
    home: HomeModel;
    places: PlaceModel[];
    cars: CarModel[];
}