export class SearchCarModel {
    pickUpId: number;
    dropOffId: number;
    fromDate: Date;
    toDate: Date;
    minTotal: number;
    maxTotal: number;
    featureIds: number[];
    seats: number;
    carModelIds: number[];
    sortType: number;
    pageSize: number;
    pageNumber: number;

    constructor() {
        this.pageNumber = 1;
        this.pageSize = 10;
        this.minTotal = 0;
        this.maxTotal = 0;
        this.carModelIds = [];
        this.featureIds = [];
    }
}