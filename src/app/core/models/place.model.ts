export class PlaceModel {
    placeId: number;
    fullAddress: string;
    latitude: number;
    longitude: number;
    phone: string;
}