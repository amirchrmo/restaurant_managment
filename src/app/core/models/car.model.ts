export class CarModel {
    carId: number;
    title: string;
    buildYear: number;
    image: string;
    modelTitle: string;
}