import { Options } from 'ng5-slider';
import { OwlOptions } from 'ngx-owl-carousel-o';

export class Configs {

    public static readonly carouselConfig: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: false,
        pullDrag: false,
        dots: false,
        navSpeed: 700,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            740: {
                items: 3
            },
            940: {
                items: 5
            }
        },
        nav: false
    }

    public static readonly rangeSliderOptions: Options = {
        floor: 0,
        ceil: 1000
    };
}