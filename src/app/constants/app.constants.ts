
export class AppConstants {

    public static ApiLocation = 'http://localhost:5000';
    public static API_ENDPOINT = 'http://localhost:5000/api/';
    public static FileLocation = 'http://localhost:5000/Uploads/';

    public static CarsLocation = AppConstants.FileLocation;

    public static EmailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    public static hours = [
        { id: 1, label: "00:00 AM" },
        { id: 2, label: "01:00 AM" },
        { id: 3, label: "02:00 AM" },
        { id: 4, label: "03:00 AM" },
        { id: 5, label: "04:00 AM" },
        { id: 6, label: "05:00 AM" },
        { id: 7, label: "06:00 AM" },
        { id: 8, label: "07:00 AM" },
        { id: 9, label: "08:00 AM" },
        { id: 10, label: "09:00 AM" },
        { id: 11, label: "10:00 AM" },
        { id: 12, label: "11:00 AM" },
        { id: 13, label: "12:00 AM" },
        { id: 14, label: "1:00 PM" },
        { id: 15, label: "2:00 PM" },
        { id: 16, label: "3:00 PM" },
        { id: 17, label: "4:00 PM" },
        { id: 18, label: "5:00 PM" },
        { id: 19, label: "6:00 PM" },
        { id: 20, label: "7:00 PM" },
        { id: 21, label: "8:00 PM" },
        { id: 22, label: "9:00 PM" },
        { id: 23, label: "10:00 PM" },
        { id: 24, label: "11:00 PM" },
    ]
}
