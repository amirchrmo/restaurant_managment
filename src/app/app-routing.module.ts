import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/auth',
        pathMatch: 'full',
    },
    {
        path: '',
        component: ContentLayoutComponent,
        children: [
            {
                path: 'auth',
                loadChildren: () =>
                    import('./modules/auth/auth.module').then((m) => m.AuthModule),
            },
            {
                path: 'home',
                loadChildren: () =>
                    import('./modules/home/home.module').then((m) => m.HomeModule),
            },
            {
                path: 'search',
                loadChildren: () =>
                    import('./modules/search/search.module').then((m) => m.SearchModule),
            }
        ],
    },
    { path: "**", redirectTo: "/home", pathMatch: "full" },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            scrollPositionRestoration: 'enabled',
            onSameUrlNavigation: 'reload'
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule { }
