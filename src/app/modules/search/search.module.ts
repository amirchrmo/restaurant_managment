import { NgModule } from '@angular/core';
import { SearchRoutingModule } from './search.routing';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { SearchComponent } from './search.component';

@NgModule({
    imports: [
        SearchRoutingModule,
        FormsModule,
        SharedModule,
    ],
    declarations: [
        SearchComponent
    ],
})
export class SearchModule { }
