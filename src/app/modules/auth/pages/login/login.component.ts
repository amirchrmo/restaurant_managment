import { Component, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { HomeDataModel } from 'src/app/core/models/homeData.model';
import { HomeService } from 'src/app/core/services/home.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', "./responsive.scss"]
})
export class LoginComponent implements OnInit {

  ngOnInit(): void {
  }

}
