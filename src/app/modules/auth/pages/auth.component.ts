import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  activeTab: string = 'login';


  constructor() { }

  ngOnInit(): void {
  }

  changeTab(tab) {
    this.activeTab = tab;
  }
}