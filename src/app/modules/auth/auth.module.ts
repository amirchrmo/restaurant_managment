import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth.routing';
import { FormsModule } from '@angular/forms';
import { AuthComponent } from './pages/auth.component';
import { SharedModule } from '../../shared/shared.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';

@NgModule({
    imports: [
        AuthRoutingModule,
        FormsModule,
        SharedModule,
        CarouselModule
    ],
    declarations: [
        AuthComponent,
        LoginComponent,
        RegisterComponent,
    ],
})
export class AuthModule { }
