import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home.routing';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { HomeComponent } from './home.component';

@NgModule({
    imports: [
        HomeRoutingModule,
        FormsModule,
        SharedModule,
    ],
    declarations: [
        HomeComponent
    ],
})
export class HomeModule { }
