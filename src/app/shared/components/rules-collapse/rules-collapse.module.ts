import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RulesCollapseComponent } from './rules-collapse.component';

@NgModule({
    declarations: [RulesCollapseComponent],
    imports: [
        FormsModule,
        CommonModule,
        RouterModule
    ],
    exports: [RulesCollapseComponent],
})
export class RulesCollapseModule { }
